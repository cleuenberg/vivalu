<?php
/**
 * Template Name: About
 */

get_header(); ?>

    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <div id="content">
            <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>

    <?php $downloads = get_posts( array(
        'post_type' => 'downloads',
        'posts_per_page' => -1
    ) ); ?>

    <?php if ( $downloads ) : ?>

        <div class="container-fluid" id="loop">
            <div class="container loop-overview loop-downloads">
                <h2><?php _e('Downloads', 'vivalu'); ?></h2>

                <?php 
                $terms = get_terms( array(
                    'taxonomy' => 'download-categories',
                    'hide_empty' => true,
                    'parent' => 0
                ) );
                ?>

                <?php if ($terms) : ?>
                    <ul class="nav nav-terms justify-content-center">

                        <li class="nav-item">
                            <a class="nav-link" data-term-element="reset-filter" href="#"><?php _e('Alle', 'vivalu'); ?></a>
                        </li>

                    <?php foreach ($terms as $term) : ?>
                        <li class="nav-item">
                            <a class="nav-link term-<?php echo $term->slug; ?>" href="#" data-term-element="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
                        </li>
                    <?php endforeach; ?>

                    </ul>
                <?php endif; ?>

                <?php foreach ( $downloads as $post ) : ?>
                    <?php setup_postdata( $post ); ?>
                    <?php get_template_part( 'sections/loop', 'download' ); ?>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>

            </div>
        </div>

        <?php wp_reset_postdata(); ?>

    <?php endif; ?>

<?php get_footer(); ?>
<?php
$term_classes = array();

$terms = get_the_terms( get_the_ID(), 'download-categories' );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
    foreach ( $terms as $term ) :
        $term_classes[] = 'term-' . $term->slug;
    endforeach;
endif;

$term_classes = implode(" ", $term_classes);
?>

<div class="d-flex flex-column align-items-stretch align-self-center filter-<?php echo $term_classes; ?>">
    <div class="<?php echo $term_classes; ?>">
        <div class="row">
            <div class="col-sm-4">
                <h3><?php the_title(); ?></h3>
            </div>
            <div class="col-sm-7">
                <?php the_excerpt(); ?>
            </div>
            <div class="col-sm-1 text-right">
                <?php $file = get_field('download-file');
                if ($file) : ?>
                    <a href="<?php echo $file['url']; ?>" title="<?php echo $file['filename']; ?> | <?php echo (number_format($file['filesize'] / 1048576, 2, ",", ".")); ?> MB" target="_blank" class="btn btn-dark btn-l btn-square text-bold"><i class="fas fa-chevron-down"></i></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php
$term_classes = array();

$terms = get_the_terms( get_the_ID(), 'product-categories' );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
    foreach ( $terms as $term ) :
        $term_classes[] = 'term-' . $term->slug;
    endforeach;
endif;

$term_classes = implode(" ", $term_classes);
?>

<div class="d-flex flex-column align-items-stretch align-self-center filter-<?php echo $term_classes; ?>">
    <div class="<?php echo $term_classes; ?>">
        <div class="row">
            <div class="col-sm-4">
                <h3><?php the_title(); ?></h3>
            </div>
            <div class="col-sm-6 col-lg-7">
                <?php the_excerpt(); ?>
            </div>
            <div class="col-sm-2 col-lg-1 text-right">
                <a href="<?php the_permalink(); ?>" class="btn btn-dark btn-xl btn-square text-bold">+</a>
            </div>
        </div>
    </div>
</div>
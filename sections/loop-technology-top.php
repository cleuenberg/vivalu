<?php if ( $post_count <= 3 ) : ?>

    <?php
    $term_classes = array();

    $terms = get_the_terms( get_the_ID(), 'technology-categories' );
    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
        foreach ( $terms as $term ) :
            $term_classes[] = 'term-' . $term->slug;
        endforeach;
    endif;

    $term_classes = implode(" ", $term_classes);
    ?>

    <?php if ( $post_count == 1 ) : ?>
        <div class="col-lg first-col">
    <?php elseif ( $post_count == 2 ) : ?>
        <div class="col-lg last-col">
    <?php endif; ?>

            <div class="<?php echo $term_classes; ?>">
                <a href="<?php the_permalink(); ?>" class="btn btn-dark btn-xl btn-square text-bold float-right">+</a>
                <h3><?php the_title(); ?></h3>
                <?php the_excerpt(); ?>
            </div>

    <?php if ( $post_count != 2 ) : ?>
        </div>
    <?php endif; ?>

<?php endif; ?>
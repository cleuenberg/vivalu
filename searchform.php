<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url( '/' ); ?>">
    <div class="form-group">
        <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Suchbegriff eingeben', 'vivalu' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Suche', 'label' ) ?>">
    </div>
    <div class="form-group">
        <a class="search-submit btn btn-dark btn-lg float-right sr-only sr-only-focusable" href="#" role="button"><i class="fas fa-chevron-right fa-fw"></i></a>
    </div>
</form>
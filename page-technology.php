<?php
/**
 * Template Name: Technology Archive
 */

get_header(); ?>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
        <div id="content">
            <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>

    <?php $technologies = get_posts( array(
        'post_type' => 'technologies',
        'posts_per_page' => -1
    ) ); ?>

    <?php if ( $technologies ) : ?>

        <?php $post_count = 1; ?>

        <div class="container-fluid" id="loop">
            <div class="container loop-highlights">

                <h2><?php _e('Unsere Highlights', 'vivalu'); ?></h2>

                <div class="row">

                    <?php foreach ( $technologies as $post ) : ?>
                        <?php setup_postdata( $post ); ?>
                        <?php set_query_var( 'post_count', $post_count ); ?>
                        <?php get_template_part( 'sections/loop', 'technology-top' ); ?>
                        <?php $post_count++; ?>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>

            <div class="container loop-overview">
                <h2><?php _e('Technologieübersicht', 'vivalu'); ?></h2>

                <?php 
                $terms = get_terms( array(
                    'taxonomy' => 'technology-categories',
                    'hide_empty' => true,
                    'parent' => 0
                ) );
                ?>

                <?php if ($terms) : ?>
                    <ul class="nav nav-terms justify-content-center">

                        <li class="nav-item">
                            <a class="nav-link" data-term-element="reset-filter" href="#"><?php _e('Alle Bereiche', 'vivalu'); ?></a>
                        </li>

                    <?php foreach ($terms as $term) : ?>
                        <li class="nav-item">
                            <a class="nav-link term-<?php echo $term->slug; ?>" href="#" data-term-element="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
                        </li>
                    <?php endforeach; ?>

                    </ul>
                <?php endif; ?>

                <?php foreach ( $technologies as $post ) : ?>
                    <?php setup_postdata( $post ); ?>
                    <?php get_template_part( 'sections/loop', 'technology' ); ?>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>

            </div>
        </div>

    <?php endif; ?>

<?php get_footer(); ?>
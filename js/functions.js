/* Custom JS Functions */
jQuery(document).ready(function($) {

    var deviceWidth = $( window ).width();
    setNaviSticky(deviceWidth);
    $( window ).resize(function() {
        deviceWidth = $( window ).width();
        setNaviSticky(deviceWidth);
    });

    // checking device rotation
    window.addEventListener("resize", function() {
        if( (deviceWidth < 992) && (window.innerHeight < window.innerWidth) ) {
            //$('#landscape-not-supported').addClass('d-block');
        } else {
            //$('#landscape-not-supported').removeClass('d-block');
        }
    }, false);

    // spying scrolling in order to set class on sticky header
    var vivaluNavbar = {

        flagAdd: true,

        elements: [],

        init: function (elements) {
            this.elements = elements;
        },

        add : function() {
            if(this.flagAdd) {
                for(var i=0; i < this.elements.length; i++) {
                    document.getElementById(this.elements[i]).className += " stucked sticky-top";
                    $('.stucked.sticky-top').fadeIn(300);
                    $('body').addClass('sticky-active');
                    $('.close-return.float-right').addClass('sticky-btn');
                }
                this.flagAdd = false;
            }
        },

        remove: function() {
            for(var i=0; i < this.elements.length; i++) {
                $('.stucked.sticky-top').attr('style','');
                document.getElementById(this.elements[i]).className =
                        document.getElementById(this.elements[i]).className.replace( /(?:^|\s)stucked sticky-top(?!\S)/g , '' );
                        $('body').removeClass('sticky-active');
                        $('.close-return.float-right').removeClass('sticky-btn');
            }
            this.flagAdd = true;
        }
    };

    if (deviceWidth > 992) {
        vivaluNavbar.init([
            "header"
        ]);
    }

    function offSetManager() {

        var yOffset = 0;
        var currYOffSet = window.pageYOffset;

        if ( !$('body.contact-collapse-shown').length && !$('body.search-collapse-shown').length ) {

            if(yOffset+450 < currYOffSet) {
                vivaluNavbar.add();
            }
            else if(currYOffSet == yOffset){
                vivaluNavbar.remove();
            }

        }
    }

    function showFireAnimation() {
        $('.master-slider .fire-permanent').show();
    }

    function showHandAnimation() {
        $('.hand-animation .wp-image-181').attr('src', '/wp-content/uploads/2018/08/Hand.gif');
        $('.hand-animation .wp-image-181').attr('srcset', '');
    }

    function killHandAnimation() {
        $('.hand-animation .wp-image-181').attr('src', '/wp-content/uploads/2018/07/hand_neu.png');
        $('.hand-animation .wp-image-181').attr('srcset', '/wp-content/uploads/2018/07/hand_neu.png 342w, /wp-content/uploads/2018/07/hand_neu-236x300.png 236w');
    }

    window.onscroll = function(e) {
        offSetManager();
    }
    
    if (deviceWidth > 992) {
        offSetManager();
    }

    /*
    var timer = null;
    $(window).scroll(function() {
        if(timer !== null) {
            clearTimeout(timer);
            showHandAnimation();
        }
        timer = setTimeout(function() {
            killHandAnimation();
        }, 150);
    });
    */

    // function to set sticky navi   
    function setNaviSticky(deviceWidth) {
        if (deviceWidth < 992) {
            $('#header').addClass('stucked sticky-top');
            $('.stucked.sticky-top').fadeIn(300);
        }
    }

    // function to show/hide terms
    function termFilterToggle(toShow) {
        if (toShow == 'reset-filter') {
            $('.loop-overview .d-none').toggleClass('d-none d-flex');
        } else {
            $('.loop-overview .d-none').toggleClass('d-none d-flex');
            $('.loop-overview .d-flex').not('.filter-term-' + toShow).addClass('d-none').removeClass('d-flex');
        }
    }

    // click event for term filtering
    $('.loop-overview .nav-terms a').click(function(e) {
        e.preventDefault();
        var toShow = $(this).attr('data-term-element');
        termFilterToggle(toShow);
    });

    // remove category link from blog section
    $('.section-blog .fl-post-grid .fl-post-grid-meta .fl-post-grid-terms a[rel="tag"]').attr('href', '#');

    // match height
    $(function() {
        $('.fl-post-grid-post .fl-post-grid-content p').matchHeight();
        $('.fl-post-carousel .fl-post-carousel-content p').matchHeight();

        $('.loop-highlights .first-col > div').matchHeight({
            target: $('.loop-highlights .last-col')
        });

        var loopFirstColHeight      = $('.loop-highlights .first-col').height();
        var loopFirstColTextHeight  = $('.loop-highlights .first-col > div h3').height() + $('.loop-highlights .first-col > div p').height();
        var loopFirstColPadding     = eval((loopFirstColHeight - loopFirstColTextHeight) / 2);
        $('.loop-highlights .first-col > div').css('padding-top',loopFirstColPadding);
    });

    // toggle body class on contact collapse
    $('#contact-collapse').on('shown.bs.collapse', function() {
        $('body').toggleClass('contact-collapse-shown');
    });
    $('#contact-collapse').on('hidden.bs.collapse', function() {
        $('body').toggleClass('contact-collapse-shown');
    });

    // toggle body class on search collapse
    $('#search-collapse').on('shown.bs.collapse', function() {
        $('body').toggleClass('search-collapse-shown');
    });
    $('#search-collapse').on('hidden.bs.collapse', function() {
        $('body').toggleClass('search-collapse-shown');
    });

    // fix showing gif animation in small screens
    if (deviceWidth < 992) {
        $(window).scroll(function() {
            var slideShowImg = $('.fl-slideshow-image-img').attr('src');
            if (slideShowImg.indexOf('-1024x487.gif') != -1) {
                slideShowImg = slideShowImg.replace("-1024x487.gif", '.gif');
                $('.fl-slideshow-image-img').attr('src', slideShowImg);
            }
        });
    }

    // show search box by default on search page
    $('body.page-template-page-search #search-collapse').collapse();

    // show contact box by default on contact page
    $('body.page-template-page-contact #contact-collapse').collapse();
});
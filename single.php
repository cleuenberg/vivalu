<?php
/**
 * Single post type
 */

get_header(); ?>

    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <?php $post_type_obj = get_post_type_object( get_post_type() ); ?>

        <div class="close-return float-right">
            <a href="<?php echo get_page_link( get_page_by_path($post_type_obj->rewrite['slug']) ); ?>" class="btn btn-light btn-xl btn-square text-bold"><span class="text-r-45">+</span></a>
        </div>

        <div id="content-header">

            <h3><?php echo $post_type_obj->labels->singular_name; ?></h3>
            <h1><?php the_title(); ?></h1>

            <div class="container">
                <?php the_excerpt(); ?>
            </div>
        </div>
        <div id="content">
            <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>
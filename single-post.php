<?php
/**
 * Single post type
 */

get_header(); ?>

    <?php if (have_posts()): while (have_posts()): the_post(); ?>

        <div class="close-return float-right">
            <a href="<?php echo home_url( '/' ); ?>#blog" class="btn btn-light btn-xl btn-square text-bold"><span class="text-r-45">+</span></a>
        </div>

        <div id="content-header">
            <div class="container">
                <h3 class="date"><?php the_date(); ?></h3>
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <div id="content">
            <div class="container">
                <?php the_post_thumbnail( 'content_full' ); ?>
                <?php the_content('', false); ?>
            </div>

            <div id="socialshare">
                <div class="container">
                    <ul class="list-inline">
                        <?php /*
                        <li class="list-inline-item">
                            <span class="btn btn-share"><?php _e('Teilen', 'vivalu'); ?></span>
                        </li>
                        */ ?>
                        <li class="list-inline-item"><a href="<?php echo vivalu_social_share( 'linkedin', get_permalink() ); ?>" target="_blank" rel="nofollow">
                            <span class="fa-stack">
                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </span>
                        </a></li>
                        <li class="list-inline-item"><a href="<?php echo vivalu_social_share( 'xing', get_permalink() ); ?>" target="_blank" rel="nofollow">
                            <span class="fa-stack">
                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                <i class="fab fa-xing fa-stack-1x"></i>
                            </span>
                        </a></li>
                        <li class="list-inline-item"><a href="<?php echo vivalu_social_share( 'facebook', get_permalink() ); ?>" target="_blank" rel="nofollow">
                            <span class="fa-stack">
                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </span>
                        </a></li>
                        <li class="list-inline-item"><a href="<?php echo vivalu_social_share( 'twitter', get_permalink() ); ?>" target="_blank" rel="nofollow">
                            <span class="fa-stack">
                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </span>
                        </a></li>
                    </ul>
                </div>
            </div>
        </div>
        
    <?php endwhile; endif; ?>

<?php get_footer(); ?>
<?php
/**
 * The template for displaying the footer
 */
?> 

        <footer id="footer">
            <div class="container">
                <div class="section section-darkgrey">
                    <h3>Curious?</h3>
                    <h2>Get in touch!</h2>
                </div>
            </div>

            <div class="container footer-middle">
                <div class="row">
                    <div class="col-sm align-self-end">
                        <?php the_field('contact_address', 'option'); ?>
                        <p><span class="text-bold">T</span> <a href="tel:<?php the_field('contact_telephone', 'option'); ?>" class="color-white"><?php the_field('contact_telephone', 'option'); ?></a><br>
                        <span class="text-bold">F</span> <a href="tel:<?php the_field('contact_fax', 'option'); ?>" class="color-white"><?php the_field('contact_fax', 'option'); ?></a><br>
                        <a href="mailto:<?php the_field('contact_email', 'option'); ?>"><?php the_field('contact_email', 'option'); ?></a></p>
                    </div>
                    <div class="col-sm align-self-end">
                        <?php the_field('contact_address_2', 'option'); ?>
                        <p><span class="text-bold">T</span> <a href="tel:<?php the_field('contact_telephone_2', 'option'); ?>" class="color-white"><?php the_field('contact_telephone_2', 'option'); ?></a><br>
                        <span class="text-bold">F</span> <a href="tel:<?php the_field('contact_fax_2', 'option'); ?>" class="color-white"><?php the_field('contact_fax_2', 'option'); ?></a><br>
                        <a class="color-red" href="mailto:<?php the_field('contact_email_2', 'option'); ?>"><?php the_field('contact_email_2', 'option'); ?></a></p>
                    </div>
                    <div class="col-sm-2 align-self-end text-right">
                        <?php if( have_rows('member_of', 'option') ): ?>
                            <?php /* <p class="text-uppercase text-bold color-red">Mitglied im</p> */ ?>
                            <ul class="list-inline member-logos">
                                <?php while ( have_rows('member_of', 'option') ) : the_row(); ?>
                                    <li class="list-inline-item">
                                        <a href="<?php the_sub_field('verbands-webseite', 'option'); ?>" target="_blank" class="color-grey" rel=“nofollow“>
                                            <?php echo wp_get_attachment_image( get_sub_field('verbands-logo', 'option'), 'medium', false, array( "class" => "img-responsive", "alt" => get_sub_field('verbandsname', 'option'), "title" => get_sub_field('verbandsname', 'option') ) ); ?>
                                        </a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm align-self-end text-right">
                        <p class="text-uppercase text-bold color-red">Follow us</p>
                        <ul class="list-inline">
                            <?php if (get_field('social_linkedin', 'option')) : ?>
                                <li class="list-inline-item">
                                    <a href="<?php the_field('social_linkedin', 'option'); ?>" target="_blank" class="color-grey">
                                        <span class="fa-stack">
                                            <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                            <i class="fab fa-linkedin-in fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_field('social_xing', 'option')) : ?>
                                <li class="list-inline-item">
                                    <a href="<?php the_field('social_xing', 'option'); ?>" target="_blank" class="color-grey">
                                        <span class="fa-stack">
                                            <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                            <i class="fab fa-xing fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_field('social_facebook', 'option')) : ?>
                                <li class="list-inline-item">
                                    <a href="<?php the_field('social_facebook', 'option'); ?>" target="_blank" class="color-grey">
                                        <span class="fa-stack">
                                            <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                            <i class="fab fa-facebook-f fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_field('social_instagram', 'option')) : ?>
                                <li class="list-inline-item">
                                    <a href="<?php the_field('social_instagram', 'option'); ?>" target="_blank" class="color-grey">
                                        <span class="fa-stack">
                                            <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                            <i class="fab fa-instagram fa-stack-1x"></i>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container footer-bottom">
                <div class="row">
                    <div class="col-sm-8">
                        <?php wp_nav_menu( array(
                            'menu'              => 'Footer Menu',
                            'menu_class'        => 'list-inline text-uppercase',
                            'container'         => false,
                            'theme_location'    => 'footer'
                        ) ); ?>
                    </div>
                    <div class="col-sm-4 text-right">
                        <p>&copy; Copyright <?php echo date('Y'); ?> | <?php echo get_bloginfo('name'); ?></p>
                    </div>
                </div>
            </div>
        </footer>

        <?php if ( shortcode_exists( 'ff' ) ) : ?>
            <div id="social-stream">
                <?php echo do_shortcode( '[ff id="1"]' ); ?>
            </div>
        <?php endif; ?>
    </div>

    <?php if ( wp_is_mobile() ) : ?>
        <?php $img_unsupported = get_field( 'img_unsupported', 'option' );
        if( !empty($img_unsupported) ): ?>
            <div id="landscape-not-supported" class="d-none">
                <div class="row">
                    <div class="col text-right">
                        <img src="<?php echo $img_unsupported['url']; ?>" alt="<?php echo $img_unsupported['alt']; ?>" class="img_unsupported" />
                    </div>
                    <div class="col-9" style="max-width:71%">
                        <h4><?php the_field( 'headline_unsupported', 'option' ); ?></h4>
                        <?php the_field( 'text_unsupported', 'option' ); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
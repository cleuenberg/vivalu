<?php
/**
 * Enqueues scripts and styles.
 *
 */
function vivalu_scripts() {
	// Bootstrap stylesheet
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );

	// Linearicons stylesheet
	wp_enqueue_style( 'linearicons-style', get_template_directory_uri() . '/css/linearicons.min.css' );

    // FontAwesome stylesheet
    wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/css/fontawesome-all.min.css', array(), '5.0.13' );

    // Theme stylesheet
    wp_enqueue_style( 'vivalu-style', get_template_directory_uri() . '/css/styles.css' );

    // Popper.js scripts
    wp_enqueue_script( 'popper-script', get_template_directory_uri() . '/js/popper.min.js', array( 'jquery' ), '1.14.3', true );

	// Bootstrap scripts
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '4.1.1', true );

    // jQuery.matchHeight scripts
    wp_enqueue_script( 'matchheight-script', get_template_directory_uri() . '/js/jquery.matchheight.min.js', array( 'jquery' ), '0.7.2', true );

	// Theme scripts
	wp_enqueue_script( 'vivalu-script', get_template_directory_uri() . '/js/functions.min.js', array( 'jquery' ), '20180531', true );
}
add_action( 'wp_enqueue_scripts', 'vivalu_scripts' );


/**
 * Dequeues scripts and styles.
 *
 */
function vivalu_disable_scripts() {
    // remove fontawesome from beaver builder
    wp_dequeue_style('font-awesome-5', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css');
}
add_filter( 'wp_print_styles', 'vivalu_disable_scripts' );


/**
 * Load translation.
 *
 */
function vivalu_load_theme_textdomain() {
    load_theme_textdomain( 'vivalu', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'vivalu_load_theme_textdomain' );


// add image sizes
function vivalu_custom_image_sizes() {
    add_image_size( 'content_full', 1320 );
    add_image_size( 'blog_thumbnail', 300, 200, true );
}
add_action('after_setup_theme', 'vivalu_custom_image_sizes');

// make image sizes selectable
function vivalu_image_size_names_choose( $sizes ) {
    return array_merge( $sizes, array(
        'content_full' => __( 'Content Image' ),
    ) );
}
add_filter( 'image_size_names_choose', 'vivalu_image_size_names_choose' );

// Registering the menus
register_nav_menus( array(
	'main' => __( 'Main Menu', 'vivalu' ),
	'footer' => __( 'Footer Menu', 'vivalu' ),
) );

// Registering the widgets
function vivalu_widgets_init() {
	register_sidebar( array(
		'name'          => 'Footer ',
		'id'            => 'product-filter',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
}
add_action( 'widgets_init', 'vivalu_widgets_init' );

// add additional menu item classes
function vivalu_nav_menu_css_class( $classes, $item, $args ) {
    if ($args->theme_location == 'footer') {
        $classes[] = "list-inline-item";
    } else if ($args->theme_location == 'main') {
        $classes[] = "nav-item";
        if( in_array('current-menu-item', $classes) ){
            $classes[] = "active";
        }
    }
    return $classes;
}
add_filter( 'nav_menu_css_class' , 'vivalu_nav_menu_css_class' , 10, 3 );

// add additional menu item link classes
function vivalu_nav_menu_link_attributes( $atts, $item, $args ) {
    if ($args->theme_location == 'footer') {
        $atts['class'] = 'color-white';
    } else if ($args->theme_location == 'main') {
        $atts['class'] = 'nav-link';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'vivalu_nav_menu_link_attributes', 10, 3 );

// Remove WP version number
function vivalu_remove_version() {
	return '';
}
add_filter('the_generator', 'vivalu_remove_version');

// Remove Yoast SEO HTML Comments
if (defined('WPSEO_VERSION')) {
  add_action('get_header', function() {
  	ob_start(function ($o) {
  		return preg_replace('/^<!--.*?[Y]oast.*?-->$/mi','',$o);
  	});
  });
  add_action('wp_head', function() { ob_end_flush(); }, 999);
}

// Add ACF Theme Options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => 'VIVALU Options'
	));
}

// remove emoji-nonsense
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// social sharing links
function vivalu_social_share( $platform, $url ) {
    switch ($platform) {
        case 'linkedin':
            return 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($url);
            break;

        case 'xing':
            return 'https://www.xing.com/spi/shares/new?url=' . urlencode($url);
            break;

        case 'facebook':
            return 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($url);
            break;
        
        case 'twitter':
            return 'https://twitter.com/share?url=' . urlencode($url);
            break;
    }
}

//enable classic editor for wordpress//
add_filter('use_block_editor_for_post','__return_false');


/**
 * Development Helper
 *
 */

// Echo preformatted debug message
function vivalu_debug( $var ) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}
<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<title>
        <?php if (is_front_page() || is_home()) :
            echo get_bloginfo('name');
        else :
            echo wp_title('') . ' | ' . get_bloginfo('name');
        endif; ?>   
    </title>

	<?php wp_head(); ?>

    <?php if ( get_field('tracking_ga', 'option') ) : ?>
        <?php if ( !is_user_logged_in() ) : ?>
            <?php the_field('tracking_ga', 'option', false); ?>
        <?php endif; ?>
    <?php endif; ?>
</head>

<?php
$body_css = '';
if ( is_singular('products') || is_singular('technologies') ) :
    $taxonomy_slug = '';
    $term_classes  = array();

    if ( is_singular('products') ) : 
        $taxonomy_slug = 'product-categories';
    elseif ( is_singular('technologies') ) :
        $taxonomy_slug = 'technology-categories';
    endif;

    $terms = get_the_terms( $post->ID, $taxonomy_slug );
    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
        foreach ( $terms as $term ) :
            $term_classes[] = 'term-' . $term->slug;
        endforeach;
    endif;

    $body_css = implode(" ", $term_classes);
endif;
?>

<body <?php body_class( $body_css ); ?>>

    <div id="wrapper" class="container-fluid">
        <header id="header" class=""><!-- class="sticky-top" -->
            <nav class="navbar navbar-expand-lg navbar-dark">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo get_bloginfo('url'); ?>">
                        <?php
                        $logo = get_field( 'logo', 'option' );
                        $logo_sticky = get_field( 'sticky_logo', 'option' );

                        if( !empty($logo) ): ?>
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="logo" width="180" height="111" />
                        <?php endif; ?>
                        <?php if( !empty($logo_sticky) ): ?>
                            <img src="<?php echo $logo_sticky['url']; ?>" alt="<?php echo $logo_sticky['alt']; ?>" class="logo_sticky" width="150" height="29" />
                        <?php endif; ?>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i><i class="fas fa-times"></i></button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">

                        <?php wp_nav_menu( array(
                            'menu'              => 'Main Menu',
                            'menu_class'        => 'navbar-nav mx-auto',
                            'container'         => false,
                            'theme_location'    => 'main'
                        ) ); ?>

                        <ul class="d-sm-flex navbar-nav mx-right">
                            <li class="nav-item">
                                <?php
                                    $pll_language_switcher = pll_the_languages(array('echo'=>false,'display_names_as'=>'slug', 'hide_current'=>1));
                                    $pll_language_switcher = str_replace('lang', 'class="nav-link" lang', $pll_language_switcher);
                                    echo $pll_language_switcher;
                                ?>
                            </li>
                            <li class="nav-item d-none d-sm-inline-block">
                                <a class="nav-link" data-toggle="collapse" href="#search-collapse" role="button" aria-expanded="false" aria-controls="search-collapse"><span class="lnr lnr-magnifier"></span></a>
                            </li>
                            <?php /*
                            <li class="nav-item d-lg-none">
                                <a class="nav-link" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Suche' ) ) ); ?>"><span class="lnr lnr-magnifier"></span></a>
                            </li>
                            */ ?>
                            <li class="nav-item d-none d-lg-inline-block">
                                <a class="nav-link" data-toggle="collapse" href="#contact-collapse" role="button" aria-expanded="false" aria-controls="contact-collapse"><span class="lnr lnr-phone-handset"></span></a>
                            </li>
                            <li class="nav-item d-lg-none">
                                <a class="nav-link" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Kontakt' ) ) ); ?>"><span class="lnr lnr-phone-handset"></a>
                            </li>
                            <?php if ( get_field('login-link_vi-tag', 'option') ) : ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php the_field('login-link_vi-tag', 'option'); ?>" target="_blank"><span class="lnr lnr-download text-r-270"></span></a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="collapse" id="search-collapse" data-parent="#wrapper">
            <div class="container">
                <div class="card card-body">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

        <div class="collapse" id="contact-collapse" data-parent="#wrapper">
            <div class="container">
                <div class="card card-body">
                    <?php
                        switch (pll_current_language()) {
                            case 'en':
                                echo do_shortcode( '[quform id="2" name="Contact"]' );
                                break;
                            
                            default:
                                echo do_shortcode( '[quform id="1" name="Kontakt"]' );
                                break;
                        }
                    ?>
                    
                    <div class="google-map">
                        <?php /* old embed code with one office:
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2499.2377124264867!2d6.766909116442076!3d51.21469647958814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b917043beccf99%3A0x6bd2b2afa8602f7d!2sVIVALU+GmbH!5e0!3m2!1sde!2sde!4v1532029033112" width="" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                        */ ?>
                        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1g58LdaFURob-_PCLXCgrDOQg7iHxgm7i&z=7" width="" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                    <div class="row">
                        <div class="col-sm align-self-end">
                            <?php the_field('contact_address', 'option'); ?>
                            <p><span class="text-bold">T</span> <a href="tel:<?php the_field('contact_telephone', 'option'); ?>" class="color-white"><?php the_field('contact_telephone', 'option'); ?></a><br>
                            <span class="text-bold">F</span> <a href="tel:<?php the_field('contact_fax', 'option'); ?>" class="color-white"><?php the_field('contact_fax', 'option'); ?></a><br>
                            <a class="color-red" href="mailto:<?php the_field('contact_email', 'option'); ?>"><?php the_field('contact_email', 'option'); ?></a></p>
                        </div>
                        <div class="col-sm align-self-end">
                            <?php the_field('contact_address_2', 'option'); ?>
                            <p><span class="text-bold">T</span> <a href="tel:<?php the_field('contact_telephone_2', 'option'); ?>" class="color-white"><?php the_field('contact_telephone_2', 'option'); ?></a><br>
                            <span class="text-bold">F</span> <a href="tel:<?php the_field('contact_fax_2', 'option'); ?>" class="color-white"><?php the_field('contact_fax_2', 'option'); ?></a><br>
                            <a class="color-red" href="mailto:<?php the_field('contact_email_2', 'option'); ?>"><?php the_field('contact_email_2', 'option'); ?></a></p>
                        </div>
                        <div class="col-sm-2 align-self-end text-right">
                            <?php if( have_rows('member_of', 'option') ): ?>
                                <?php /* <p class="text-uppercase text-bold color-red">Mitglied im</p> */ ?>
                                <ul class="list-inline member-logos">
                                    <?php while ( have_rows('member_of', 'option') ) : the_row(); ?>
                                        <li class="list-inline-item">
                                            <a href="<?php the_sub_field('verbands-webseite', 'option'); ?>" target="_blank" class="color-grey" rel=“nofollow“>
                                                <?php echo wp_get_attachment_image( get_sub_field('verbands-logo', 'option'), 'medium', false, array( "class" => "img-responsive", "alt" => get_sub_field('verbandsname', 'option'), "title" => get_sub_field('verbandsname', 'option') ) ); ?>
                                            </a>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div class="col-sm align-self-end text-right">
                            <p class="text-uppercase text-bold color-red">Follow us</p>
                            <ul class="list-inline">
                                <?php if (get_field('social_linkedin', 'option')) : ?>
                                    <li class="list-inline-item">
                                        <a href="<?php the_field('social_linkedin', 'option'); ?>" target="_blank" class="color-grey">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_field('social_xing', 'option')) : ?>
                                    <li class="list-inline-item">
                                        <a href="<?php the_field('social_xing', 'option'); ?>" target="_blank" class="color-grey">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                                <i class="fab fa-xing fa-stack-1x"></i>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_field('social_facebook', 'option')) : ?>
                                    <li class="list-inline-item">
                                        <a href="<?php the_field('social_facebook', 'option'); ?>" target="_blank" class="color-grey">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_field('social_instagram', 'option')) : ?>
                                    <li class="list-inline-item">
                                        <a href="<?php the_field('social_instagram', 'option'); ?>" target="_blank" class="color-grey">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
                                                <i class="fab fa-instagram fa-stack-1x"></i>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>